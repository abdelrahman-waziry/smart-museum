function setFocus() {
    var focusElm = document.getElementById('firstElement');
    focusElm.focus();
}

function selectFocused() {
   var focused = document.activeElement;
   focused.click();
}


function next() {
    var el = document.getElementById('firstElement');
    var nextElm = el.nextElementSibling;
    el.removeAttribute('id','firstElement')
    nextElm.setAttribute('id', 'firstElement');
    nextElm.focus();
    var currentElm = document.getElementsByClassName('navItem')[3];
    currentElm.removeAttribute('id', 'firstElement');
    currentElm.setAttribute('id', 'lastElement');
}

function prev() {
    var el = document.getElementById('lastElement');
    var nextElm = el.previousElementSibling;
    el.removeAttribute('id','lastElement')
    nextElm.setAttribute('id', 'lastElement');
    nextElm.focus();
    var currentElm = document.getElementsByClassName('navItem')[0];
    currentElm.removeAttribute('id', 'lastElement');
    currentElm.setAttribute('id', 'firstElement');
}


$(document).ready(function(){
  $('#stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10);
   
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  $('#stars li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10);
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    
  });
  
  
});

